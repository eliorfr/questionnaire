import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, createMuiTheme, StylesProvider } from '@material-ui/core/styles';
import { jssPreset } from '@material-ui/core/styles';
import { create as jssCreate } from 'jss';
import { SnackbarProvider } from 'notistack';
import CloseIcon from '@material-ui/icons/Close';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DayJsUtils from '@date-io/dayjs';

import MainApp from './components/main_app';
import { withReduxProvider } from './redux/store';
import { snackbarRef } from './utils/snackbar';

const jss = jssCreate({ plugins: [...jssPreset().plugins] });

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#3263E2',
      contrastText: '#fff',
    },
  },
  typography: {
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
});

const App = () => {
  const closeSnackbar = () => {
    snackbarRef.current.closeSnackbar();
  };

  return (
    <StylesProvider jss={jss}>
      <MuiPickersUtilsProvider utils={DayJsUtils}>
        <ThemeProvider theme={theme}>
          <SnackbarProvider
            ref={snackbarRef}
            autoHideDuration={2500}
            maxSnack={1}
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            action={() => <CloseIcon key="1" onClick={closeSnackbar} style={{ cursor: 'pointer' }} />}>
            <BrowserRouter>
              <MainApp />
            </BrowserRouter>
          </SnackbarProvider>
        </ThemeProvider>
      </MuiPickersUtilsProvider>
    </StylesProvider>
  );
};

export default withReduxProvider(App);
