import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';

import { useReduxState } from '../hooks/use_redux_state';
import { Stage } from '../types/stage';
import Stages from '../components/stages';

const useStyles = makeStyles({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    padding: 25,
  },
});

const Home: React.FC<{}> = () => {
  const [stages] = useReduxState({ key: 'stages' });
  const classes = useStyles();

  return (
    <div className={classes.container}>
      {stages.map((stage: Stage, i: number) => (
        <Stages key={stage.title} stage={stage} index={i} />
      ))}
    </div>
  );
};

export default Home;
