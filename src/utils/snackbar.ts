import { createRef } from 'react';
import { SnackbarMessage, OptionsObject } from 'notistack';

export interface EnqueueSnackbar {
  enqueueSnackbar: (message: SnackbarMessage, options?: OptionsObject) => OptionsObject['key'] | null;
}

export const snackbarRef: React.RefObject<EnqueueSnackbar> = createRef();
