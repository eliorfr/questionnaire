import { compareTwoStrings } from 'string-similarity';
import _ from 'lodash';
import { Dictionary } from '../types/global';

const getSimilarity = (query: string, option: string): number => {
  option = option.replace(/[\W_]+/g, ' ');
  const similarities: Array<number> = option
    .split(' ')
    .filter((s) => s.length >= 3)
    .concat(option)
    .map((r) => compareTwoStrings(r, query));
  const maxNumber = _.max(similarities);
  return maxNumber || -1;
};

interface SearchOption {
  startsWith: boolean;
  includes: boolean;
  similarity: number;
  option: string | Dictionary<any>;
  oldOption: string | Dictionary<any>;
}

export const searchResults = (
  query: string,
  options: Array<string | Dictionary<any>>,
  key?: string,
): string | Dictionary<any> => {
  if (!query) {
    return [];
  }
  query = query.toLowerCase();
  let searchOptions: Array<SearchOption> = options.map((option) => {
    let optionName: string = typeof option === 'string' ? option : !!key && option[key];
    optionName = optionName.toLowerCase();
    const similarity = getSimilarity(query, optionName);
    const startsWith = optionName.startsWith(query);
    const includes = query.split(' ').every((w) => optionName.includes(w));
    return { startsWith, includes, similarity, option, oldOption: option };
  });

  searchOptions = searchOptions.filter((o) => o.startsWith || o.includes || o.similarity >= 0.4);
  searchOptions = _.orderBy(searchOptions, ['startsWith', 'includes', 'similarity'], ['desc', 'desc', 'desc']);
  return searchOptions.map((m) => m.oldOption);
};
