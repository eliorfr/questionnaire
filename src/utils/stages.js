import { patientQuestions } from '../utils/Questions.json';
import { store } from '../redux/store';

export const getNextStage = (currentStage, index, answer) => {
  if (answer === currentStage?.childItems?.[0]?.parentAnswer) {
    return currentStage.childItems;
  } else {
    if (currentStage?.childItems?.[0]?.parentAnswer) {
      const { stages } = store.getState();
      store.dispatch({ type: 'stages', payload: stages.slice(0, index + 1) });
    }
  }
  if (!patientQuestions[index + 1]) {
    return;
  }
  return patientQuestions[index + 1];
};
