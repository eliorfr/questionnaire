import { useEffect, useState } from 'react';

const getWindowDimensions = () => {
  const { innerWidth: width, innerHeight: height } = window;
  return { width, height };
};

export const useWindowDimensions = () => {
  const defaultHeight = getWindowDimensions().height;
  const defaultWidth = getWindowDimensions().width;
  const [height, setHeight] = useState(defaultHeight);
  const [width, setWidth] = useState(defaultWidth);

  useEffect(() => {
    const handleResize = () => {
      const { height: newHeight, width: newWidth } = getWindowDimensions();
      setHeight(newHeight);
      setWidth(newWidth);
    };
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return { height, width };
};

export default useWindowDimensions;
