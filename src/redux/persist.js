import { persistReducer, createTransform } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import reducers from './reducers';

const whitelist = [];

// const getLastCookieValue = (key: string) => {
//   return document.cookie
//     .split(';')
//     ?.filter?.((c) => c.includes(`${key}=`))
//     .pop()
//     ?.split?.(`${key}=`)
//     .pop();
// };

const inboundTransformer = (inboundState, key) => {
  switch (key) {
    default:
      return inboundState;
  }
};

const outboundTransformer = (outboundState, key) => {
  switch (key) {
    default:
      return outboundState;
  }
};

const transformConfig = { whitelist };
const setTransform = createTransform(inboundTransformer, outboundTransformer, transformConfig);

const persistConfig = { key: 'root', storage, whitelist, transforms: [setTransform] };
export const persistedReducer = persistReducer(persistConfig, reducers);
