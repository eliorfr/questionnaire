import { combineReducers } from 'redux';

import { patientQuestions } from '../utils/Questions.json';

const createReducer = (actionType, initialState) => (state = initialState, action) => {
  switch (action.type) {
    case actionType:
      return action.payload;
    default:
      return state;
  }
};

const reducersDefaultValues = {
  stages: [...patientQuestions],
};

const reducers = {};
Object.keys(reducersDefaultValues).forEach((key) => {
  reducers[key] = createReducer(key, reducersDefaultValues[key]);
});

const appReducer = combineReducers(reducers);

const rootReducer = (state, action) => {
  if (action.type === 'reset') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
