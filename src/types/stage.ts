export interface Stage {
  title: string;
  type: string;
  options?: string[];
  childItems?: ChildItems;
}

export interface ChildItems extends Stage {
  parentAnswer: 'string';
}
