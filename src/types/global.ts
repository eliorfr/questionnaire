export interface Env {
  [key: string]: string | undefined;
}

declare global {
  interface Window {
    env: Env;
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    safari: object;
    StyleMedia: any;
  }
}

export interface Dictionary<T> {
  [Key: string]: T;
}
