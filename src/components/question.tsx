import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';

import BodyText from './body_text';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    borderRadius: 4,
    alignItems: 'center',
  },
  question: {
    marginRight: 15,
    fontWeight: 'bold',
    color: '#6B6B6B',
  },
});

interface Props {
  question: string;
}

const Question: React.FC<Props> = ({ question, children }) => {
  const classes = useStyles();

  return (
    <div data-testid={question} className={classes.container}>
      <BodyText className={classes.question}>{question}</BodyText>
      {children}
    </div>
  );
};

export default Question;
