import React from 'react';
import { Redirect } from 'react-router-dom';

import ProtectedRoute from './protected_route';

export const getDefaultRoute = (): string => {
  return '/home';
};
console.log('object');
const DefaultRoute = () => {
  const defaultRoute = getDefaultRoute();
  return <ProtectedRoute path="*" render={() => <Redirect to={defaultRoute} />} />;
};

export default DefaultRoute;
