import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedRoute = ({ ...props }) => {
  const notAuth = false;

  if (notAuth) {
    return <Redirect {...props} to={`/auth?continueWith=${window.location.pathname}`} />;
  }

  return <Route {...props} />;
};

export default ProtectedRoute;
