import React, { ComponentType } from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundary from '../utils/error_boundary';
import ProtectedRoute from './protected_route';

const importRoute = (importPathFunction: any): ComponentType => {
  const Loadable = React.lazy(importPathFunction);
  return (props: any) => (
    <ErrorBoundary>
      <Loadable {...props} />
    </ErrorBoundary>
  );
};

const Home = importRoute(() => import('../pages/home'));
const DefaultRoute = importRoute(() => import('./default_route'));

const Router = () => (
  <Switch>
    <ProtectedRoute exact path="/home" component={Home} />
    <ProtectedRoute exact path="*" component={DefaultRoute} />
  </Switch>
);

export default Router;
