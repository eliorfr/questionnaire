import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField } from '@material-ui/core';

const useStyles = makeStyles({
  select: {
    width: 220,
    marginLeft: 8,
  },
});

interface Props {
  options: string[];
  onChange: (value: string) => void;
}

const SelectQuestion: React.FC<Props> = ({ options, onChange }) => {
  const classes = useStyles();

  const handleChange = (option: string) => {
    onChange(option);
  };

  return (
    <TextField className={classes.select} select variant="outlined" label="select">
      {options?.map((option) => (
        <MenuItem key={option} value={option} onClick={() => handleChange(option)}>
          {option}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default SelectQuestion;
