import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';

import Question from './question';
import TextFieldQuestion from '../components/text_field_question';
import RadioQuestion from '../components/radio_question';
import SelectQuestion from '../components/select_question';
import { getNextStage } from '../utils/stages';
import { useReduxState } from '../hooks/use_redux_state';
import { Stage } from '../types/stage';

interface CssProps {
  marginLeft: number;
}

const useStyles = makeStyles({
  stage: {
    marginLeft: ({ marginLeft }: CssProps) => marginLeft,
    marginBottom: 20,
  },
});

interface Props {
  stage: Stage;
  index: number;
}

const Stages: React.FC<Props> = ({ stage, index }) => {
  const [stages, setStages] = useReduxState({ key: 'stages' });
  const marginLeft = stages.slice(0, index).filter((s: Stage) => s.childItems && s.title).length * 30;
  const classes = useStyles({ marginLeft });

  const handleChange = (answer: string) => {
    const nextStage = getNextStage(stage, index, answer);
    const isExist = stages.some((s: Stage) => s.title === nextStage?.title);
    if (!nextStage || isExist) {
      return;
    }
    if (Array.isArray(nextStage)) {
      setStages(stages.concat(nextStage));
    } else {
      setStages([...stages, nextStage]);
    }
  };

  return (
    <div className={classes.stage}>
      <Question question={stage.title}>
        {stage.type === 'number' && <TextFieldQuestion onChange={handleChange} />}
        {stage.type === 'radio' && <RadioQuestion onChange={handleChange} />}
        {stage.type === 'select' && <SelectQuestion options={stage.options} onChange={handleChange} />}
      </Question>
    </div>
  );
};

export default Stages;
