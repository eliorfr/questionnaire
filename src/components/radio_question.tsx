import React from 'react';
import { FormControlLabel, Radio, RadioGroup } from '@material-ui/core';

interface Props {
  onChange: (value: string) => void;
}

const RadioQuestion: React.FC<Props> = ({ onChange }) => {
  const handleChange = (_: React.ChangeEvent<HTMLInputElement>, value: string) => {
    onChange(value);
  };

  return (
    <RadioGroup row onChange={handleChange}>
      <FormControlLabel value="1" control={<Radio />} label="Yes" />
      <FormControlLabel value="2" control={<Radio />} label="No" />
    </RadioGroup>
  );
};

export default RadioQuestion;
