import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles({
  textField: {
    width: 220,
    marginLeft: 8,
  },
});

interface Props {
  onChange: (value: string) => void;
}

const TextFieldQuestion: React.FC<Props> = ({ onChange }) => {
  const classes = useStyles();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const answer = event.target.value;
    onChange(answer);
  };

  return <TextField className={classes.textField} onChange={handleChange} variant="outlined" type="number" />;
};

export default TextFieldQuestion;
