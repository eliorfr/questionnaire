import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Fade from '@material-ui/core/Fade';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(() => ({
  loading: {
    position: 'fixed',
    transform: 'translate(-50%, -50%)',
    top: '50%',
    left: '50%',
    zIndex: 1,
  },
}));

interface Props {
  inAction?: boolean;
  delay?: string;
}

const Loading: React.FC<Props> = ({ inAction = true, delay = '200ms' }) => (
  <div className={useStyles().loading}>
    <Fade in={inAction} unmountOnExit style={{ transitionDelay: inAction ? delay : '0ms' }}>
      <CircularProgress />
    </Fade>
  </div>
);

export default Loading;
