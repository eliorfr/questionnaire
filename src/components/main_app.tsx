import { Suspense } from 'react';
import Box from '@material-ui/core/Box';
import makeStyles from '@material-ui/core/styles/makeStyles';

import Loading from './loading';
import Router from './router';

const useClasses = makeStyles({
  container: {
    flex: 1,
    position: 'relative',
    display: 'flex',
    backgroundColor: '#F8F8F8',
    overflow: 'hidden',
  },
});

const MainApp = () => {
  const classes = useClasses();

  return (
    <Box display="flex" className={classes.container}>
      <Suspense fallback={<Loading />}>
        <Router />
      </Suspense>
    </Box>
  );
};

export default MainApp;
