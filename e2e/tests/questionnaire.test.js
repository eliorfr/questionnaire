/* global page */

describe('Questionnaire', () => {
  it('check initial items are rendered', async () => {
    await page.goto('http://localhost:3000/home');

    expect(page.url()).toContain('home');
    const ageSelector = '[data-testid="Age"]';
    const isUsingHeartMedicationsSelector = '[data-testid="Are you using any heart medications ?"]';
    await page.waitForSelector(ageSelector);

    const age = await page.$(ageSelector);
    expect(age).not.toBe(null);

    const isUsingHeartMedications = await page.$(isUsingHeartMedicationsSelector);
    expect(isUsingHeartMedications).not.toBe(null);
  });
});
